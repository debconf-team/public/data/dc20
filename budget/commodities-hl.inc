commodity USD
   format USD 1,000.00

commodity EUR
   format EUR 1,000.00

commodity CHF
   format CHF 1,000.00

commodity BRL
   format BRL 1.000,00

commodity ILS
   format ILS 1.000,00
